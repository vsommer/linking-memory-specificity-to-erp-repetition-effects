function [ID] = bebo_ids(type,age)
% BEBO IDs
% Input:
% - type (STRING): all, beh, eeg, final
% - age (STRING): ya, oa, both (adults), ch, all 
% Output:
% - ID (DOUBLE if age == ya,oa,both or CELL if age == ch,all

% which IDs
if strcmp(type,'all') % all available IDs
        
    YA = [3109,3110,3111,3112,3113,3114,3115,3116,3117,3118,3119,3120,3121,3122,3123,3124,3125,3126,3127,3128,3129,3130,3131,3132, ...
        3205,3206,3207,3208,3209,3210,3211,3212,3213,3214,3215,3216,3217,3218,3219,3220,3222];

    OA = [4109,4110,4111,4112,4113,4114,4115,4116,4118,4119,4120,4121,4122,4123,4124,4125,4126,4127,4128,4129,4131,4132,4133,4134,4135,4136,4137,4138, ...
        4207,4208,4209,4210,4211,4212,4213,4214,4215,4216,4217,4218,4219,4220,4221,4222,4223,4224,4225,4226,4227,4228,4229,4230,4231,4232,4233,4234,4235,4236,4237,4238,4239];
    
    CH = {'01IIG19','03JBJ25','08DSV17','02CSS25', '02DSW27','02RML05', '02SLL01', '04CJC10', '05SSM12', '06KRB20', '06MHK14', '06NSN29', '07BSJ05','07KNJ16', '08MKV10', '09KRE29', '10KRA30', '10SSC13', '11SWC30', '12CLP06', '12SLZ15', '12UKV20'};

elseif strcmp(type,'beh')  % IDs with complete behavior data (without 4234, 4235)
    
    YA = [3109,3110,3111,3112,3113,3114,3115,3116,3117,3118,3119,3120,3121,3122,3123,3124,3125,3126,3127,3128,3129,3130,3131,3132, ...
        3205,3206,3207,3208,3209,3210,3211,3212,3213,3214,3215,3216,3217,3218,3219,3220,3222];

    OA = [4109,4110,4111,4112,4113,4114,4115,4116,4118,4119,4120,4121,4122,4123,4124,4125,4126,4127,4128,4129,4131,4132,4133,4134,4135,4136,4137,4138, ...
        4207,4208,4209,4210,4211,4212,4213,4214,4215,4216,4217,4218,4219,4220,4221,4222,4223,4224,4225,4226,4227,4228,4229,4230,4231,4232,4233,4236,4237,4238,4239];

    CH = {'01IIG19','03JBJ25','08DSV17','02CSS25', '02DSW27','02RML05', '02SLL01', '04CJC10', '05SSM12', '06KRB20', '06MHK14', '06NSN29', '07BSJ05','07KNJ16', '08MKV10', '09KRE29', '10KRA30', '10SSC13', '11SWC30', '12CLP06', '12SLZ15', '12UKV20'};
    
elseif strcmp(type,'eeg') % IDs with complete eeg data

    YA = [3109,3110,3111,3112,3113,3114,3115,3117,3118,3119,3120,3121,3122,3123,3124,3125,3126,3127,3128,3129,3130,3131,3132, ...
        3205,3206,3207,3208,3209,3211,3212,3213,3214,3215,3216,3217,3218,3219,3220,3222];
    
    OA = [4109,4110,4111,4112,4113,4114,4115,4116,4118,4119,4120,4121,4122,4123,4124,4125,4126,4127,4128,4129,4131,4132,4133,4134,4135,4136,4137,4138, ...
        4207,4208,4209,4210,4211,4212,4213,4214,4215,4216,4217,4218,4219,4220,4221,4222,4223,4224,4225,4227,4228,4229,4230,4231,4232,4233,4236,4238,4239];
    
    CH = {'01IIG19','03JBJ25','08DSV17','02CSS25', '02DSW27','02RML05', '02SLL01', '04CJC10', '05SSM12', '06KRB20', '06MHK14', '06NSN29', '07BSJ05','07KNJ16', '08MKV10', '09KRE29', '10KRA30', '10SSC13', '11SWC30', '12CLP06', '12SLZ15', '12UKV20'};

elseif strcmp(type,'final') % IDs finally included, i.e. all excluded based on beh, eeg data etc 

    YA = [3109,3110,3111,3112,3113,3114,3115,3117,3118,3119,3120,3121,3122,3123,3124,3125,3126,3127,3128,3129,3130,3131,3132, ...
        3205,3206,3207,3208,3209,3211,3212,3213,3214,3215,3216,3217,3218,3219,3220,3222];
    
    OA = [4109,4110,4111,4112,4113,4114,4115,4116,4118,4119,4120,4121,4122,4124,4125,4126,4127,4128,4129,4131,4132,4133,4134,4135,4136,4137,4138, ...
        4207,4208,4209,4210,4211,4212,4213,4214,4215,4216,4217,4218,4219,4220,4221,4222,4223,4224,4225,4227,4228,4230,4231,4232,4233,4236,4238,4239];
    
    CH = {'01IIG19','03JBJ25','08DSV17','02CSS25', '02DSW27','02RML05', '02SLL01', '04CJC10', '05SSM12', '06KRB20', '06MHK14', '06NSN29', '07BSJ05','07KNJ16', '08MKV10', '09KRE29', '10KRA30', '10SSC13', '11SWC30', '12SLZ15', '12UKV20'};

    YA2 = {'01SDS28','01SLM07','02AKM27','02BKA25','02IBC23','02MFK07','02PSD14','03DWS08','03NSJ02','03RHT19','03UVM02','04BGL09','07MKD17','08CLC22','08DWP06','08WFN15','09HBE02','09KSA02','10SLL12','11BKL21','11CPA17','11IKA09','12PTM11'};
        
elseif strcmp(type,'control')
    
    YA = {'01SDS28','01SLM07','02AKM27','02BKA25','02IBC23','02MFK07','02PSD14','03DWS08','03NSJ02','03RHT19','03UVM02','04BGL09','07MKD17','08CLC22','08DWP06','08WFN15','09HBE02','09KSA02','10SLL12','11BKL21','11CPA17','11IKA09','12PTM11'};
    
end

% which age group(s)
if strcmp(age,'ya') || strcmp(age,'YA')
    
    ID = YA;
    
elseif strcmp(age,'oa') || strcmp(age,'OA')
     ID = OA;
     
elseif strcmp(age,'both') % ADULTS
    
    ID = [YA,OA];
    
elseif strcmp(age,'ch') || strcmp(age,'CH') % 
    
    ID = CH;
    
elseif strcmp(age,'ya2') || strcmp(age,'YA2')
    
    ID = YA2;
    
elseif strcmp(age,'all')
    
    YA = num2cell(YA);
    OA = num2cell(OA);
    
    ID = [YA,OA,CH];
    
elseif strcmp(age,'allall')
    
    YA = num2cell(YA);
    OA = num2cell(OA);
    
    ID = [YA,YA2,OA,CH];
end
    
    
end