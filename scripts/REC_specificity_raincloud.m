% analyze memory performance (category / item) and plot age comparison
% (raincloud plots)

% VRS 2019

pn.root = '/home/mpib/vsommer/';
addpath(genpath([pn.root 'scripts']))


%% load data

load([pn.root 'data/behavior/REC_performances_Kids.mat'])

old2old_k = all_old2old(:,2:end); % don't need 1 (sum)
sim2lure_k = all_sim2lure(:,2:end);
old2lure_k = all_old2lure(:,2:end);
sim2old_k = all_sim2old(:,2:end);
old2foil_k = all_old2foil;
sim2foil_k = all_sim2foil;
new2foil_k = all_new2foil;
clear all_old2old all_sim2lure all_sim2old all_old2lure all_old2foil all_new2foil all_new2lure all_new2old all_sim2foil


load([pn.root 'data/behavior/REC_performances_YA.mat'])

old2old_ya = all_old2old(:,2:end);
sim2lure_ya = all_sim2lure(:,2:end);
old2lure_ya = all_old2lure(:,2:end);
sim2old_ya = all_sim2old(:,2:end);
old2foil_ya = all_old2foil;
sim2foil_ya = all_sim2foil;
new2foil_ya = all_new2foil;
clear all_old2old all_sim2lure all_sim2old all_old2lure all_old2foil all_new2foil all_new2lure all_new2old all_sim2foil

load([pn.root 'data/behavior/REC_performances_OA.mat'])

old2old_oa = all_old2old(:,2:end);
sim2lure_oa = all_sim2lure(:,2:end);
old2lure_oa = all_old2lure(:,2:end);
sim2old_oa = all_sim2old(:,2:end);
old2foil_oa = all_old2foil;
sim2foil_oa = all_sim2foil;
new2foil_oa = all_new2foil;
clear all_old2old all_sim2lure all_sim2old all_old2lure all_old2foil all_new2foil all_new2lure all_new2old all_sim2foil


%% decide what memory measure
% CH: 240 trials incl. 80 targets, 80 lures, 80 foils, with 20 N/HR/HE/HRE,
% respectively. YA/OA: 480 trials incl. 160 t/l/f, with 40 N/HR/HE/HRE,
% respectively

% EITHER item memory (IM)
correct_prop_k = (old2old_k + sim2lure_k) ./40; 
incorr_prop_k = (old2foil_k + sim2foil_k) ./ 80 + (old2lure_k + sim2old_k) ./40;

correct_prop_ya = (old2old_ya + sim2lure_ya) ./80; 
incorr_prop_ya = (old2foil_ya + sim2foil_ya) ./160 + (old2lure_ya + sim2old_ya) ./80;

correct_prop_oa = (old2old_oa + sim2lure_oa) ./80; 
incorr_prop_oa = (old2foil_oa + sim2foil_oa) ./160 + (old2lure_oa + sim2old_oa) ./80 ;

% OR category memory but incorret item memory (CMO)
correct_prop_k = (old2lure_k + sim2old_k) ./40; 
incorr_prop_k = (old2foil_k + sim2foil_k) ./ 80;

correct_prop_ya = (old2lure_ya + sim2old_ya) ./80; 
incorr_prop_ya = (old2foil_ya + sim2foil_ya) ./160;

correct_prop_oa = (old2lure_oa + sim2old_oa) ./80; 
incorr_prop_oa = (old2foil_oa + sim2foil_oa) ./160;


% OR LDI (Ngo, 2017)
correct_prop_k = sim2lure_k ./20;
incorr_prop_k = old2lure_k ./80;

correct_prop_ya = sim2lure_ya ./40;
incorr_prop_ya = old2lure_ya ./160;

correct_prop_oa = sim2lure_oa ./40;
incorr_prop_oa = old2lure_oa ./160;


% difference
prop_perf_k = correct_prop_k - incorr_prop_k;
prop_perf_ya = correct_prop_ya - incorr_prop_ya;
prop_perf_oa = correct_prop_oa - incorr_prop_oa;



%% average across conditions

k_dat = mean(prop_perf_k,2);
ya_dat = mean(prop_perf_ya,2);
oa_dat = mean(prop_perf_oa,2);


%% plot 

bw = 0.06;

figure
hold on
h1 = raincloud_plot('X', ya_dat, 'box_on', 1,'bandwidth', bw, 'color', [0 0 0],'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .55, 'dot_dodge_amount', .15, 'box_col_match', 1);
h1 = raincloud_plot('X', oa_dat, 'box_on', 1,'bandwidth', bw, 'color', [138/244 44/244 135/244], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .75, 'dot_dodge_amount', .15, 'box_col_match', 1);
h1 = raincloud_plot('X', k_dat, 'box_on', 1,'bandwidth', bw, 'color', [0 159/244 227/244], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .35, 'dot_dodge_amount', .15, 'box_col_match', 1);
ax = gca;
%ax.XLim = XLim;
ax.YLim = [-4 4];
ax.YTick =[];


