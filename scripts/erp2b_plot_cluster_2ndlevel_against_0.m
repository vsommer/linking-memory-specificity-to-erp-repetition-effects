% plot cluster stat results first vs second encoding (see erp5b)
% http://www.fieldtriptoolbox.org/tutorial/cluster_permutation_timelock/#plotting-the-results-1

% VRS Feb 2019

restoredefaultpath;
clear all; close all; %pack; clc;

pn.root = '/home/mpib/vsommer/'; 
pn.fun = [pn.root 'scripts/functions/']; 
pn.eeg = [pn.root 'data/eeg/'];

addpath([pn.fun 'fieldtrip-20190329'])
addpath([pn.fun 'export_fig'])

% settings
group = 'CH';
task = 'Enc';
cond = 'avgcond'; % for data
contrast = '_first_vs_second_';

saveon = 0; % save figures

% for electrode information
if strcmp(group,'CH')
    load([pn.eeg '02CSS25_ENC_config.mat'])
else
    load([pn.eeg '3109_Enc_config.mat'])
end

if strcmp(group,'YA')
    numsubj = '39';
elseif strcmp(group,'OA')
    numsubj = '55';
elseif strcmp(group,'CH')
    numsubj = '21';
end

% define CM colormap
addpath([pn.fun 'cbrewer/']); %colormap designer
% create colormap
Cm = cbrewer('div', 'RdBu', 128); % diverging! 'RdBu'
Cm = flipud(Cm); % puts red on top, blue at the bottom


%% load

% data 
load([pn.eeg numsubj '_' group '_GA_' task '_' cond '_first_present.mat'],'ga_first');
load([pn.eeg numsubj '_' group '_GA_' task '_' cond '_second_present.mat'],'ga_second');

% inconsistent naming...
if strcmp(cond,'avgcond')
    cond = 'allcond';
end

% stat from second-level analysis
load([pn.root 'stats/' numsubj group '_second_level' contrast cond stimpairs '.mat'])
stat = stat2;

    
%% prepare layout
cfg = [];
cfg.elec = config.elec;
%correct some flipping of the elec
cfg.elec.pnt(:,1) = -config.elec.pnt(:,2);
cfg.elec.pnt(:,2) = config.elec.pnt(:,1);
layout = ft_prepare_layout(cfg, ga_first);


%% get time intervals and electrodes of sign. clusters 

% find sign clusters
pos_cluster_pvals = [stat.posclusters(:).prob];
pos_signif_clust = find(pos_cluster_pvals < stat.cfg.alpha);
pos = ismember(stat.posclusterslabelmat, pos_signif_clust);
 
neg_cluster_pvals = [stat.negclusters(:).prob];
neg_signif_clust = find(neg_cluster_pvals < stat.cfg.alpha);
neg = ismember(stat.negclusterslabelmat, neg_signif_clust);


% find electrodes, time points from each cluster
for sign_clust = 1:length(pos_signif_clust)
    [m,n] = find(stat.posclusterslabelmat == sign_clust);
    pos_elecs{sign_clust} = stat.label(unique(m)); 
    pos_tps{sign_clust} = stat.time(unique(n)); 
end

for sign_clust = 1:length(neg_signif_clust)
    [m,n] = find(stat.negclusterslabelmat == sign_clust);
    neg_elecs{sign_clust} = stat.label(unique(m)); 
    neg_tps{sign_clust} = stat.time(unique(n)); 
end


% plot ERP of sign. electrodes and highlight sign. time, and respective
% topo averaged over time points

% pos
for sign_clust = 1:length(pos_signif_clust)
    
    % ERP
    cfg = []; 
    cfg.channel = pos_elecs{sign_clust}; 
    cfg.xlim = [-0.2 1.7];
    cfg.linewidth = 1;

    figure
    ft_singleplotER(cfg, ga_first,ga_second);
    ax = gca;
    ax.XAxisLocation = 'origin';
    ax.YAxisLocation = 'origin';
    ax.FontSize = 14;
    ax.YMinorTick = 'on';
    ax.XMinorTick = 'on';
    title('')
    legend('first','second')
    ylabel('\muV')
    xlabel('time (s)')    
    ax = gca;
    % highlight sign time interval
    rectangle('Position',[min(pos_tps{sign_clust}) ax.YLim(1) max(pos_tps{sign_clust})-min(pos_tps{sign_clust}) sumabs(ax.YLim)]) %, 'FaceColor',[0.8 0.8 0.8], 'EdgeColor','none')

    if saveon
        export_fig([pn.root 'ERP_2ndlevel_pos_cl' num2str(sign_clust) '_' numsubj '_' group contrast cond '_stimpairs_ms'], '-pdf','-transparent')    
    end
    
    % TOPO
    cfg = [];
    cfg.layout = layout;
    cfg.parameter = 'stat';
    cfg.xlim = [min(pos_tps{sign_clust}) max(pos_tps{sign_clust})]; 
    cfg.highlight          = 'on';
    cfg.highlightchannel   = pos_elecs{sign_clust}; 
    cfg.highlightsize = 16 ;
    cfg.marker = 'on';
    cfg.markerstyle = '.';
    cfg.markersize = 2;
    cfg.colorbar = 'yes';
    cfg.colormap = Cm;  % load before! see above
    cfg.comment = 'no';
    cfg.zlim = 'maxabs';
    cfg.gridscale = 100;
    
    hFigure = figure('Color',[1 1 1]); axes1 = axes('Parent',hFigure,'Layer','top','FontSize',20);
    hold(axes1,'all'); ft_topoplotER(cfg,stat)
    title('')
    
    if saveon 
        export_fig([pn.root 'Topo_2ndlevel_pos_cl' num2str(sign_clust) '_' numsubj '_' group contrast cond '_stimpairs_ms'], '-pdf','-transparent')    
    end
end

% neg
for sign_clust = 1:length(neg_signif_clust)
    
    % ERP
    cfg = []; 
    cfg.channel = neg_elecs{sign_clust}; 
    cfg.xlim = [-0.2 1.7];
    cfg.linewidth = 1;

    figure
    ft_singleplotER(cfg, ga_first,ga_second);
    ax = gca;
    ax.XAxisLocation = 'origin';
    ax.YAxisLocation = 'origin';
    ax.FontSize = 14;
    ax.YMinorTick = 'on';
    ax.XMinorTick = 'on';
    title('')
    legend('first','second')
    ylabel('\muV')
    xlabel('time (s)')    
    ax = gca;
    % highlight sign time interval
    rectangle('Position',[min(neg_tps{sign_clust}) ax.YLim(1) max(neg_tps{sign_clust})-min(neg_tps{sign_clust}) sumabs(ax.YLim)]) %, 'FaceColor',[0.8 0.8 0.8], 'EdgeColor','none')

    if saveon
        export_fig([pn.root 'ERP_2ndlevel_neg_cl' num2str(sign_clust) '_' numsubj '_' group contrast cond '_stimpairs_ms'], '-pdf','-transparent')    
    end
    
    % TOPO
    cfg = [];
    cfg.layout = layout;
    cfg.parameter = 'stat';
    cfg.xlim = [min(neg_tps{sign_clust}) max(neg_tps{sign_clust})]; 
    cfg.highlight          = 'on';
    cfg.highlightchannel   = neg_elecs{sign_clust}; 
    cfg.highlightsize = 16 ;
    cfg.marker = 'on';
    cfg.markerstyle = '.';
    cfg.markersize = 2;
    cfg.colorbar = 'yes';
    cfg.colormap = Cm;  % load before! see above
    cfg.comment = 'no';
    cfg.zlim = 'maxabs';
    cfg.gridscale = 100;
    
    hFigure = figure('Color',[1 1 1]); axes1 = axes('Parent',hFigure,'Layer','top','FontSize',20);
    hold(axes1,'all'); ft_topoplotER(cfg,stat)
    title('')
    
    if saveon 
        export_fig([pn.root 'Topo_2ndlevel_neg_cl' num2str(sign_clust) '_' numsubj '_' group contrast cond '_stimpairs_ms'], '-pdf','-transparent')    
    end
end

