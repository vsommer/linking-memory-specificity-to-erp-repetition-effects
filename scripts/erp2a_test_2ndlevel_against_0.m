%% Second level (between-subjects within-group) analysis
% test first-level t-values against 0, controlling for multiple comparison
% by conducting non-parametric, cluster-based, random permutation analysis

% VRS Feb 2019

clear all
pn.root = '/home/mpib/vsommer/'; 

group = 'CH';
contrast = '_first_vs_second_';

if strcmp(group,'CH')
    snum = '21';
elseif strcmp(group,'YA')
    snum = '39';
elseif strcmp(group,'OA')
    snum = '55';
end

% load stat from first-level analysis
load([pn.root 'data/stats/' snum group '_first_level' contrast 'allcond_stimpairs.mat'])

% make null distribution
null_distr = stat;
null_distr.stat = zeros(size(stat.stat));

% electrode information
if strcmp(group,'CH')
    load([pn.root 'data/eeg/02CSS25_ENC_config.mat'])
else
    load([pn.root 'data/eeg/3109_Enc_config.mat']) 
end

cfg = [];
cfg.method      = 'distance'; 
cfg.elec        = config.elec;
neighbours      = ft_prepare_neighbours(cfg, stat); % define neighbouring channels

% stats
cfg = [];
cfg.channel     = 'all';
cfg.neighbours  = neighbours; % defined as above
cfg.latency     = [-0.1 1.6];
cfg.avgovertime = 'no';
cfg.parameter   = 'stat';
cfg.method      = 'montecarlo'; %'analytic';
cfg.statistic   = 'ft_statfun_indepsamplesT';
cfg.alpha       = 0.025;
cfg.correctm    = 'cluster'; %'no';
cfg.clusteralpha = 0.05; 
cfg.clusterstatistic = 'maxsum';
cfg.correcttail = 'prob';
cfg.minnbchan        = 2; % minimal neighbouring channels
cfg.numrandomization = 10000;

% design matrix
Nsub = size(stat.stat,1);
cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable (condition)


%% run 
stat2 = ft_timelockstatistics(cfg,stat,null_distr); 
stat2.df = stat.df;

save([pn.root 'data/stats/' snum group '_second_level' contrast 'allcond_stimpairs'],'stat2')



