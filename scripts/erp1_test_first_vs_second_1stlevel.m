%% First level analysis
% On the first (within-subject) level, we compared the ERPs of every encoding trial in which an object item
% was presented for the first time with the ERPs of the respective second presentation using two-sided paired
% sample t-tests.

% Input: individual subject ERP data (not published)
% Output: Stat file first-level t-values

% VRS Feb 2019

restoredefaultpath;
clear all; close all; pack; clc;

pn.root = '/home/mpib/vsommer/'; % tardis
pn.fun = [pn.root 'scripts/functions/']; % tardis

% add function folders folders for analysis
THG_addpath_fnct_common(pn.fun,0)
THG_addpath_fnct_thg(pn.fun)
addpath([pn.fun 'fieldtrip-20190329'])
ft_defaults
addpath(genpath([pn.root 'scripts']))

task = 'Enc'; % Encoding condition
group = 'CH';
subjects = bebo_ids('final',group); % input 1: all,beh,eeg,final; input 2: ch,ya,oa

if strcmp(group,'CH')
    channum = 64;
else
    channum = 60;
end

stat = zeros(length(subjects),channum,1376); % will contain all stat results

 
for subj = 1:length(subjects)
    
    if strcmp(group,'CH')
        curr = subjects{subj}
    else
        curr = num2str(subjects(subj))
    end
    
    % individual eeg data
    load([pn.root 'BEBO/EEG-Data/Analyses/' curr '/eeg/' curr '_Enc_allcond.mat'],'data');

    pres1 = 1;
    pres2 = 2;

    pairs = []; % list to save all pairs       
    for stim1 = 1:size(data.trial,2)-1
        
        % stim is not in pair list yet and it's the first presentation
        if ~ismember(stim1,pairs) && data.trialinfo2{stim1,9} == pres1 
            curr_pair = data.trialinfo2{stim1,7}; % current stim item
            pairs(stim1,1) = stim1; 

            for stim2 = stim1+1:size(data.trial,2)
                
                % it's the current pair and its second presentation
                if strcmp(data.trialinfo2{stim2,7},curr_pair) && data.trialinfo2{stim2,9} == pres2 
                     pairs(stim1,2) = stim2;
                end
            end
        end
    end
    pairs(~all(pairs,2),:) = []; % remove zeros (no partner)

    % separate into first & second presentation data
    cfg = [];
    cfg.trials = pairs(:,1); % all first presentations;
    data_1 = ft_selectdata(cfg,data);

    cfg = [];
    cfg.trials = pairs(:,2); % all second presentations;
    data_2 = ft_selectdata(cfg,data);

    
    %% stats
    
    cfgtest = [];
    cfgtest.statistic   = 'ft_statfun_depsamplesT'; 
    cfgtest.channel     = {'all', '-IOL','-RHEOG','-LHEOG','-A1'};
    cfgtest.latency     = 'all';
    cfgtest.avgovertime = 'no';
    cfgtest.avgoverchan = 'no';
    cfgtest.parameter   = 'trial';
    cfgtest.method      = 'analytic';
   
    % number of trials in each condition
    Ntrial1 = size(data_1.trial,2);
    Ntrial2 = size(data_2.trial,2);

    cfgtest.design(1,1:Ntrial1+Ntrial2)  = [ones(1,Ntrial1) 2*ones(1,Ntrial2)];
    cfgtest.design(2,1:Ntrial1+Ntrial2)  = [1:Ntrial1 1:Ntrial2];  
    cfgtest.ivar                = 1; % the 1st row in cfg.design contains the independent variable (condition)
    cfgtest.uvar                = 2; % the 2nd row in cfg.design contains the trial number

    % run
    warning('off','getdimord:warning_dimord_could_not_be_determined')
    tmp_stat = ft_timelockstatistics(cfgtest,data_1,data_2); 

    stat(subj,:,:) = tmp_stat.stat;
        
    dfs(subj) = tmp_stat.df;
    
end


tmp_stat = rmfield(tmp_stat,{'stat','df','critval','prob','mask'});
tmp_stat.dimord = 'subj_chan_time';
tmp_stat.stat = stat;
clear stat
stat = tmp_stat;
stat.df = dfs; % save df for every subj!

save([pn.root 'data/stats/' num2str(size(subjects,2)) group '_first_level_first_vs_second_allcond_stimpairs'],'stat')

