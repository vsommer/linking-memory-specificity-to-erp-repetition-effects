% correlate individual t-values from first level analysis (within sign. clusters from second level)
% first vs second presentation with individual performance

clear all
pn.root = '/home/mpib/vsommer/';

addpath(genpath([pn.root 'scripts\']))

cond = 'allcond';

z_transf = 1 % z-transform t-vals in stat (mwb_t2z)
z_score_effect = 1 % Standardize t/z-values from stat (Z = zscore(X))
z_score_perf = 1 % standardize performance

% choose performance measure
perf = 'item' % item memory (IM)
%perf = 'onlycat' % category memory only (CMO)
%perf = 'ldi2' % lure discrimination index (LDI) 

chance_adj = 1; % adjust for chance level for item or cat memory 


%% load performance
for g = {'CH','YA','OA'}
    group = cell2mat(g);
    subjects = bebo_ids('final',group); 

    if strcmp(group,'CH')
        load([pn.root 'data\behavior\REC_performances_Kids.mat'],'perform_rel')
        lia = ismember(perform_rel(2:end-2,1),subjects);
    else
        load([pn.root 'data\behavior\REC_performances_Adults.mat'],'perform_rel')
        lia = ismember(cell2mat(perform_rel(2:end-4,1)),subjects);
    end

    % only performance of subjects with complete eeg
    lia = logical([1; lia]); % add 1 for first row
    perform = perform_rel(lia,:);

    % o old (target), s sim (lure), n new (foil)
    o2o = 2;
    o2s = 3;
    o2n = 4;
    s2o = 5;
    s2s = 6;
    s2n = 7;
    n2o = 8;
    n2s = 9;
    n2n = 10;
    
    if strcmp(perf,'item')
        % item memory / "specificity" (corrected for tendency to say old or sim AND unspecific memories where only category remembered): (old2old + sim2sim) - sim2old - old2sim - old2new - sim2new
        p.(group) = (cell2mat(perform(2:end,o2o)) + cell2mat(perform(2:end,s2s)) - cell2mat(perform(2:end,s2o)) - cell2mat(perform(2:end,o2s)))./2 - cell2mat(perform(2:end,o2n)) - cell2mat(perform(2:end,s2n));   
        
        if chance_adj
            % chance adjustment! 
            performance.(group) = performance.(group) + 2/9;
        end
        
    elseif strcmp(perf,'onlycat')
        % "unspecific" only category memory (corrected for tendency to say old or sim): (sim2old + old2sim) - old2new - sim2new 
        performance.(group) = (cell2mat(perform(2:end,s2o)) + cell2mat(perform(2:end,o2s))) ./2 - cell2mat(perform(2:end,o2n)) - cell2mat(perform(2:end,s2n));   

    elseif strcmp(perf,'ldi2')
        % lure discrimination index sim2lure - old2lure (Ngo, 2017)
        performance.(group) = (cell2mat(perform(2:end,s2s)) - cell2mat(perform(2:end,o2s)));
        
    end
end

%% standardize (z-transform) performance

if z_score_perf
    performance.CH = zscore(performance.CH);
    performance.YA = zscore(performance.YA);
    performance.OA = zscore(performance.OA);
end

%% load t/z values
load([pn.root 'data\stats\t_z_vals_extracted_from_clusters_all_groups.mat'])

if ~z_transf
    cl_dat = cl_dat_t;
else
    cl_dat = cl_dat_z; 
end
cl_dat.REc.YA = cl_dat.RE1.YA; % rename

% standardize
if z_score_effect
    cl_dat.RSc.CH = zscore(cl_dat.RSc.CH);
    cl_dat.RSc.YA = zscore(cl_dat.RSc.YA);
    cl_dat.RSc.OA = zscore(cl_dat.RSc.OA);
    cl_dat.REc.YA = zscore(cl_dat.REc.YA);
    cl_dat.REc.OA = zscore(cl_dat.REc.OA);
end


%% correlate all effects with performance 

% want to create figure?
figi = 1;

% new composite effects
for effect = {'RSc','REc'}
    display(cell2mat(effect))
    
    groups = fieldnames(cl_dat.(cell2mat(effect)))'; % all groups that show this effect
    
    for g = groups % correlate for all available groups
        display(cell2mat(g))
        [r,p] = corr(cl_dat.(cell2mat(effect)).(cell2mat(g)), performance.(cell2mat(g)))
        
        if figi
            figure
            scatter(cl_dat.(cell2mat(effect)).(cell2mat(g)), performance.(cell2mat(g)),'filled')
            lsline
            xlabel('z-values within sign. cluster')
            ylabel('performance')
            title([cell2mat(g) ' ' cell2mat(effect) ' r = ' num2str(round(r,2)) ', p = ' num2str(round(p,3))])
        end
    end
end

